//
//  DetailView.swift
//  H4X0R
//
//  Created by Laurynas Kapacinskas on 2021-07-05.
//

import SwiftUI

struct DetailView: View {
    
    let url: String?
    
    var body: some View {
        WebView(urlString: url)
    }

}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(url: "https://google.com")
    }
}


