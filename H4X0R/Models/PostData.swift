//
//  PostData.swift
//  H4X0R
//
//  Created by Laurynas Kapacinskas on 2021-07-05.
//

import Foundation

struct Results: Decodable {
    let hits: [Post]
}

struct Post: Decodable, Identifiable {
    var id: String {
        return objectID
    }
    let objectID: String
    let points: Int
    let title: String
    let url: String?
}
